
#include <SoftwareSerial.h>
#include <string.h>

// Initialize the pins for input/output data in GSM Mobile Module

SoftwareSerial gprsSerial(7, 8);

// The mobile number which is used to send and receive control message to control the water motor on/off 

char mobile_number[] = "71584109";

// The cloud url string buffer 

char url_str_buf[250]; 

const int motor_one = 4;

boolean value = true, condition = true;
int i = 0, j = 0;

//Sensor AO pin to Arduino pin A0

//const int sensorValue = A0;          

//Variable to store the incoming sensor data

int sensorValue;

char DATA_ONE, DATA_TWO;

char dataSERIAL[10];

// Control messages which is sent to motor by the user to switch on/off the motor

char mot_one[] = {'M','O','T','O','N'};
char mot_two[] = {'M','O','T','O','F'};

// Starts the initialization of setting up Arduino Uno Micro controller and GSM Mobile Module.

void setup()
{
// Initialize baud rate for GSM module

  gprsSerial.begin(19200);  
  
// Initialize baud rate for Serial monitor

  Serial.begin(19200);       

  Serial.println("Configuring GSM Module / SIM800L ...");
  delay(2000);
  Serial.println("Configuration is done! ... ");
  gprsSerial.flush();
  Serial.flush();
  
// Attach or Detach from GPRS service

  gprsSerial.println("AT+CGATT?");  
  delay(100);
  toSerial();

// Checks for mobile bearer 1 is open or not

  gprsSerial.println("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"");    
  delay(2000);
  toSerial();
  
// Mobile network APN is connected or not properly

  gprsSerial.println("AT+SAPBR=3,1,\"APN\",\"www.internet.mtelia.dk\"");    
  delay(2000);
  toSerial();

// Enables bearer 1

  gprsSerial.println("AT+SAPBR=1,1");     
  delay(2000);
  toSerial();

  pinMode(motor_one, OUTPUT);
    
    delay(2000);

  configuration_initial();
    
    Serial.println("System is Initialized to Continue: ");
    Serial.println("    ");
    delay(1000);
}

// Enters into the loop to read the sensor data continuously

void loop()
{

// Initialize Http service

   gprsSerial.println("AT+HTTPINIT"); 
   delay(2000); 
   toSerial();
   
// read the input on Analog pin A0:
//int sensorValue = analogRead(A0); 

   sensorValue = analogRead(A0);
  
// print out the value you read from water sensor
  
   Serial.println(sensorValue);  

// delay in between sensor readings for stability

   delay(100);        

   cloud();

//cloud function starts here

   delay(1000);

label: 
  
while(sensorValue<=450)
   {
    
    Serial.println("Present Water level is : 0mm - Empty!"); 
    digitalWrite(motor_one, LOW);
    delay(3000);
	
//digitalWrite(motor_one, HIGH);
    
    sensorValue=analogRead(A0);
    Serial.println(sensorValue);
    delay(100); 
    cloud();
   }
   
// Sending Required Water Level Sensor Message to the User Mobile ( Starts the operation )

    gprsSerial.println("AT+CMGF=1"); 
    delay(2000);
    
    gprsSerial.println("AT+CMGS=\"71584109\"\r");
    delay(1000);          
          
    gprsSerial.print("Required Water Level is Reached in the Rice Field:");
    Serial.print("Required Water Level is Reached in the Rice Field:");
    
    Serial.println(sensorValue);
    gprsSerial.println(sensorValue);
    
    Serial.println("    ");
    gprsSerial.print((char)26);

    delay(2000);
  
   Serial.println("You are not controlled so Controlled the Motor System itself: ");
   digitalWrite(motor_one, HIGH);
   sensorValue = analogRead(A0);
   Serial.println("-------sensorVaue-----");
   delay(300);
   cloud();

// Sending Required Water Level Sensor Message to the User Mobile ( Starts the operation )

   gprsSerial.println("AT+CMGF=1"); 
    delay(2000);
    
    gprsSerial.println("AT+CMGS=\"71584109\"\r");
    delay(1000);
                    
    gprsSerial.print("Required Water Level is Reached So The motor is autostopped: ");
    Serial.print("Required Water Level is Reached So The motor is autostopped: ");
    
    Serial.print(sensorValue);
    gprsSerial.println(sensorValue);
    
    Serial.println("    ");
    gprsSerial.print((char)26);

// To take off the sensor from the water to check whether the control is going back or not 
	
   delay(10000); 

  sensorValue = analogRead(A0);
  delay(2000);
 
 if(sensorValue <= 450)
 {
  goto label;
 } 
      
    while(true)
    {
       read_message();
       sensorValue = analogRead(A0);
        if(sensorValue<=450)
        {
          loop();
        }
       goto label;
       
    }

}

void cloud()
   {
// initialize Http service

   gprsSerial.println("AT+HTTPINIT"); 
   delay(2000); 
   toSerial();
   char * baseurl = "http://data.sparkfun.com/input/RMxygzzZJOFzYV07oZyx?private_key=lzEBqrr4WmtM5qbj7d8a&";
   char * colname = "vand";
   
//char url_str_buf[250];
   
   sprintf(url_str_buf, "AT+HTTPPARA=\"URL\",\"%s%s=%d\"", baseurl, colname, sensorValue);  // set the Http URL with corresponding water sensor values
   gprsSerial.println(url_str_buf);
   delay(2000);
   toSerial();
   
// set up the Http action where type 0 = GET, 1 = POST, 2 = HEAD

   gprsSerial.println("AT+HTTPACTION=0"); 
   delay(6000);
   toSerial();

// read server response as HttP Read

   gprsSerial.println("AT+HTTPREAD"); 
   delay(1000);
   toSerial();
   gprsSerial.println("");

// The HTTP service is to be terminated ( Terminate Http service)

   gprsSerial.println("AT+HTTPTERM"); 
   toSerial();
   delay(300);

   gprsSerial.println("");

   }

void toSerial()
{
  while(gprsSerial.available()!=0)
  {
    Serial.write(gprsSerial.read());
  }
}

void configuration_initial()
{
// Read data from analog pin and store it to value variable

    sensorValue = analogRead(A0);   

// Sending Required Water Level Sensor Message to the User Mobile ( Starts the operation )

    gprsSerial.println("AT+CMGF=1"); 
    delay(300);

    gprsSerial.println("AT+CMGR=?");
    delay(300);
    gprsSerial.println("AT+CNMI=2,2,0,0");
    delay(300);
    gprsSerial.println("Configuration of GSM with Arduino is finished: ");
    delay(300); 
}

void read_message()
{
  entry:
    if(gprsSerial.available()>0)
      {
        char DATA_ONE = gprsSerial.read();
          if(DATA_ONE == '@')
          { 
            digitalWrite(13, HIGH);
            
            Serial.println("Motor_One is in an action to control:");
             Serial.println("    ");
              while(true)
                { 
                  if(gprsSerial.available()>0)
                    {
					// Motor_On Mode enters here
					
                      char DATA_TWO = gprsSerial.read(); 
                      dataSERIAL[j] = DATA_TWO;
                      j++;
                        if(DATA_TWO == '\n')
                          {
                           
                           Serial.println("Reading the given controlling message: ");
                            Serial.println("    ");
                              for(int i = 0; i <= j; i++)
                                {
                                  Serial.print(dataSERIAL[i]);
                                }
                                
                              motor_on(); 
                              motor_off();
                              
                            delay(500);
                            digitalWrite(13, LOW);
                              for(int i = 0; i <= j; i++)
                                {
                                  dataSERIAL[i] = 0;
                                  DATA_TWO = 0;
                                  DATA_ONE = 0;
                                }
                            j = 0;
                            goto entry;
                          } // closes all here
                    } // end of the second if
                } // while close
          } // at
      } // serial available

     return;
}
void motor_on()
{
  if(dataSERIAL[0] == mot_one[0] && dataSERIAL[1] == mot_one[1] && dataSERIAL[2] == mot_one[2] && dataSERIAL[3] == mot_one[3] && dataSERIAL[4] == mot_one[4])
    {
      digitalWrite(motor_one, LOW);
      Serial.println("Motor_One is ON Mode Now: ");
	  
	// Sending Message back to the Mobile
	
      gprsSerial.println("AT+CMGF=1");  
        delay(2000);
      gprsSerial.println("AT+CMGS=\"+4571584109\"\r");
        delay(1000);                
      gprsSerial.println("Motor_One is Started Now: ");
      Serial.println("    ");
      gprsSerial.print((char)26);
      sensorValue = analogRead(A0);
    }
    if(sensorValue <= 450)
 {
  loop();
 } 

}

void motor_off()
{
  if(dataSERIAL[0] == mot_two[0] && dataSERIAL[1] == mot_two[1] && dataSERIAL[2] == mot_two[2] && dataSERIAL[3] == mot_two[3] && dataSERIAL[4] == mot_two[4])
    {
      digitalWrite(motor_one, HIGH);
      Serial.println("Motor_One is OFF Mode Now:");
      Serial.println("    ");

	// Sending Message back to the Mobile
	
      gprsSerial.println("AT+CMGF=1");  
      delay(2000);
      gprsSerial.println("AT+CMGS=\"+4571584109\"\r");
      delay(1000);
      gprsSerial.println("Motor_One is Stopped Now: ");
      Serial.println("    ");
      gprsSerial.print((char)26);
    }
    
}
